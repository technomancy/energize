# ENERGIZE!

You are a petty officer aboard the USS Lakota assigned to the transporter
room. You learn the basics of transporter operation in a series of simple
assignments, and as you progress in rank you come across more and more
difficult challenges.

## Levels:

* cargo box of quadrotriticale (tutorial)
* one person
* several people
* falling person?
* person wearing an unauthorized weapon?
* reversing a transporter accident
* Tuvix??

We should see if we can incorporate some backstory for the characters
beaming in.

## Game mechanics options:

Falling particles come down and must be placed in order to fill a silhouette
of the character beaming in. As the particles fall, they phase in and out.
At any point when it's phased in, you can lock the particle in place, unlike
in classic tetris where blocks only stop falling when they land on another
block. You have a limited number of particles to place before you run out,
but you don't need to get it 100% perfect.

 OR:

sokoban-like arrangement of particles?

## Art needed

* [ ] title screen
* [ ] mission briefing (LCARS padd w/ commander portrait?)
* [ ] transporter pad with control panel
* [ ] external shots between missions?
* [ ] various characters being beamed in

https://p.hagelb.org/galaxy-transporter.jpg

## modes

* [ ] intro
* [ ] briefing
* [ ] play
* [ ] lose
* [ ] win

## credits

* [LÖVE](https://love2d.org) Copyright © 2006-2018 LOVE Development Team, distributed under the zlib license
* [Fennel](https://github.com/bakpakin/Fennel) Copyright © 2016-2018 Calvin Rose and contributors, distributed under the MIT/X11 License
* [lume](https://github.com/rxi/lume) Copyright © 2015-2018 rxi, distributed under the MIT/X11 License
* [polywell](https://git.sr.ht/~technomancy/polywell) Copyright © 2015-2019 Phil Hagelberg, distributed under LGPLv3
* [fonts](https://github.com/wrstone/fonts-startrek) distributed under GPLv3
